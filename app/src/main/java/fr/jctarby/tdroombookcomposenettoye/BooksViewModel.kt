package fr.jctarby.tdroombookcomposenettoye

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import fr.jctarby.tdroombookcomposenettoye.data.model.Book
import fr.jctarby.tdroombookcomposenettoye.data.model.Comment
import fr.jctarby.tdroombookcomposenettoye.data.repository.BookRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.launch

class BooksViewModel(private val repository: BookRepository) : ViewModel() {

    // liste des livres
    // la variable est publique, mais on ne peut pas la modifier de l'extérieur car c'est un flux venant de Room.
    // On peut juste récupérer son contenu, puis faire des opérations dessus comme filter, map....
    var books: Flow<List<Book>> = repository.getAllBooksFromRoom()

    // un livre (utilisé pour add, update....)
    var book by mutableStateOf(Book(0, "", ""))

    var bookAndComments: Flow<Map<Book, List<Comment>>> = emptyFlow()

    /*
    // Autre écriture plus classique :
    fun getBook(id: Int) {
        viewModelScope.launch {
            book = repo.getBookFromRoom(id)
        }
    }*/
    fun getBook(id: Int) = viewModelScope.launch {
        book = repository.getBookFromRoom(id)
    }

    suspend fun addBook(book: Book): Long {
        return repository.addBookToRoom(book)
    }

    fun updateBook(book: Book) = viewModelScope.launch {
        repository.updateBookInRoom(book)
    }

    fun deleteBook(book: Book) = viewModelScope.launch {
        repository.deleteBookFromRoom(book)
    }

    fun updateTitle(title: String) {
        book = book.copy(
                title = title
        )
    }

    fun updateAuthor(author: String) {
        book = book.copy(author = author)
    }

    suspend fun addComment(comment: Comment) = viewModelScope.launch(context = Dispatchers.IO) {
        repository.addCommentToRoom(comment)
    }

    suspend fun getBookWithComments(bookId: Long) : Map<Book, List<Comment>>
    {
        return repository.getBookWithCommentsFromRoom(bookId)
    }


    // La Factory pour créer le viewmodel avec le repository en paramètre.
    class BooksViewModelFactory(private val repository: BookRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(BooksViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return BooksViewModel(repository) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}
