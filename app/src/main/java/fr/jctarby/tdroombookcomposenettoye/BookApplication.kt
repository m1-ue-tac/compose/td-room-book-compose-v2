package fr.jctarby.tdroombookcomposenettoye

import android.app.Application
import fr.jctarby.tdroombookcomposenettoye.data.dao.BookDao
import fr.jctarby.tdroombookcomposenettoye.data.db.BookDataBase
import fr.jctarby.tdroombookcomposenettoye.data.repository.BookRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

// Classe Application (lancée en premier, cf. Manifest) et qui crée le repo, le DAO et la BDD.
// Ces derniers sont donc des Singletons.
class BookApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())
    val database: BookDataBase by lazy { BookDataBase.getDatabase(this) }
    val bookDao: BookDao by lazy { database.bookDao() }
    val repository: BookRepository by lazy { BookRepository(bookDao) }
}
