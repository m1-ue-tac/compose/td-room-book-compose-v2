package fr.jctarby.tdroombookcomposenettoye

class Utils {

    companion object {
        //Room
        const val BOOK_TABLE = "book_table"
        const val COMMENT_TABLE = "comment_table"
        const val BOOK_DATABASE = "book_database"
    }
}