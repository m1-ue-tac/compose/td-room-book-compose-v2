package fr.jctarby.tdroombookcomposenettoye.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import fr.jctarby.tdroombookcomposenettoye.Utils.Companion.BOOK_TABLE

@Entity(tableName = BOOK_TABLE)
data class Book(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        val title: String,
        val author: String,
)
