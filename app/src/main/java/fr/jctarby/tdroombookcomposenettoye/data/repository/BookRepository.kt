package fr.jctarby.tdroombookcomposenettoye.data.repository

import fr.jctarby.tdroombookcomposenettoye.data.dao.BookDao
import fr.jctarby.tdroombookcomposenettoye.data.model.Book
import fr.jctarby.tdroombookcomposenettoye.data.model.Comment
import kotlinx.coroutines.flow.Flow

// permet de définir un nouveau type sans créer une classe de données car juste une liste à gérer
typealias Books = List<Book>

// Le Repository est une classe, mais créée une seule fois lors du lancement de l'application (cf. BookApplication)
class BookRepository(private val bookDao: BookDao) {

    fun getAllBooksFromRoom(): Flow<Books> {
        return bookDao.getBooks()
    }

    suspend fun getBookFromRoom(id: Int): Book {
        return bookDao.getBook(id)
    }

    suspend fun addBookToRoom(book: Book): Long {
        return bookDao.addBook(book)
    }

    suspend fun updateBookInRoom(book: Book) {
        return bookDao.updateBook(book)
    }

    suspend fun deleteBookFromRoom(book: Book) {
        return bookDao.deleteBook(book)
    }

    suspend fun addCommentToRoom(comment: Comment) {
        return bookDao.addComment(comment)
    }

    suspend fun getBookWithCommentsFromRoom(bookid:Long): Map<Book, List<Comment>>{
        return bookDao.getBookWithComments(bookid)
    }
}
