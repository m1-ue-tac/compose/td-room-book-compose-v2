package fr.jctarby.tdroombookcomposenettoye.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import fr.jctarby.tdroombookcomposenettoye.Utils.Companion.BOOK_TABLE
import fr.jctarby.tdroombookcomposenettoye.Utils.Companion.COMMENT_TABLE
import fr.jctarby.tdroombookcomposenettoye.data.model.Book
import fr.jctarby.tdroombookcomposenettoye.data.model.Comment
import fr.jctarby.tdroombookcomposenettoye.data.repository.Books
import kotlinx.coroutines.flow.Flow

@Dao
interface BookDao {

    // ------------- les livres ------------

    //    @Query("SELECT * FROM $BOOK_TABLE ORDER BY id ASC")
    @Query("SELECT * FROM $BOOK_TABLE ORDER BY id DESC")
    fun getBooks(): Flow<Books>

    @Query("SELECT * FROM $BOOK_TABLE WHERE id = :id")
    suspend fun getBook(id: Int): Book

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addBook(book: Book): Long

    @Update
    suspend fun updateBook(book: Book)

    @Delete
    suspend fun deleteBook(book: Book)

    @Insert
    suspend fun insertAllBooks(books: List<Book>)

    @Query("DELETE FROM $BOOK_TABLE")
    fun deleteAllBooks()

    // ---------- les commentaires ----------
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addComment(comment: Comment)

    @Query("DELETE FROM $COMMENT_TABLE")
    fun deleteAllComments()

    @Query("SELECT * FROM $BOOK_TABLE " +
            "JOIN $COMMENT_TABLE ON $BOOK_TABLE.id = $COMMENT_TABLE.bookId " +
            "WHERE $BOOK_TABLE.id = :bookId")
    suspend fun getBookWithComments(bookId: Long): Map<Book, List<Comment>>

}