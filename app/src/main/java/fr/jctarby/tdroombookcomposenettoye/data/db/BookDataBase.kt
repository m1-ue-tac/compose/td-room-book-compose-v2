package fr.jctarby.tdroombookcomposenettoye.data.db

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import fr.jctarby.tdroombookcomposenettoye.Utils.Companion.BOOK_DATABASE
import fr.jctarby.tdroombookcomposenettoye.data.dao.BookDao
import fr.jctarby.tdroombookcomposenettoye.data.model.Book
import fr.jctarby.tdroombookcomposenettoye.data.model.Comment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(
        entities = [Book::class, Comment::class],
        version = 1,
        exportSchema = false
)
abstract class BookDataBase : RoomDatabase() {
    abstract fun bookDao(): BookDao

    companion object {
        @Volatile
        private var instance: BookDataBase? = null

        fun getDatabase(context: Context): BookDataBase {

            // uniquement pour les tests, on vide la BDD à chaque fois (run from scratch), donc pas besoin
            // de gérer le changement de numéro de version
            // Ne pas oublier de retirer cette ligne à la fin !!!!
            if (context.deleteDatabase(BOOK_DATABASE)) Log.d("JC", "RESET de la BDD OK") else Log.d("JC", "RESET de la BDD PAS OK !!!")

            return instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                        context.applicationContext,
                        BookDataBase::class.java,
                        BOOK_DATABASE
                )
                        // Cette ligne provoque la suppression et la recréation de la base de données en cas de migration
                        // .fallbackToDestructiveMigration()
                        // Cette ligne provoque (en théorie) le préremplissage de la BDD, mais KO pour le moment :-(
                        // .addCallback(BookDatabaseCallback())
                        .build()
            }
        }
    }

    /**
     * Plutôt que de créer les données dans l'activité, on pourrait tout à fait le faire ici.
     */
    //TODO pour le moment, ça ne marche pas :-(
    private class BookDatabaseCallback() : Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            Log.d("BDD", "création de la bdd")
            instance?.let { database ->
//                scope.launch {
                CoroutineScope(Dispatchers.IO).launch {
                    val bookDao = database.bookDao()
                    bookDao.deleteAllBooks()
                    val initialBooks = listOf(
                            Book(id = 0, title = "Book 1", author = "Author 1"),
                            Book(id = 0, title = "Book 2", author = "Author 2"),
                            // Ajoutez d'autres livres ici
                    )
                    bookDao.insertAllBooks(initialBooks)
                }
            }
        }
    }
}