package fr.jctarby.tdroombookcomposenettoye.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import fr.jctarby.tdroombookcomposenettoye.Utils.Companion.COMMENT_TABLE

@Entity(tableName = COMMENT_TABLE)
data class Comment(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        val comment: String,
        val pseudo: String,
        val bookId: Int //  l'ID du livre associé
)