package fr.jctarby.tdroombookcomposenettoye

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import fr.jctarby.tdroombookcomposenettoye.data.model.Book
import fr.jctarby.tdroombookcomposenettoye.data.model.Comment
import fr.jctarby.tdroombookcomposenettoye.ui.theme.TDRoomBookComposeNettoyeTheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {

    private var lastId: Long = 0

    // le viewmodel doit connaître le repository. On utilise donc une Factory.
    val booksViewModel: BooksViewModel by viewModels {
        BooksViewModel.BooksViewModelFactory((application as BookApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TDRoomBookComposeNettoyeTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {

                    LaunchedEffect(key1 = "toto") {
                        lifecycleScope.launch {
                            for (i in 1..10) {
                                val book = Book(id = 0, title = "Book $i", author = "Author $i")
                                val idBook: Long = booksViewModel.addBook(book)
                                lastId = idBook

                                Log.d("BDD", "insert livre $i - $idBook")

                                for (j in 1..3) {
                                    val comment = Comment(0, "comment $i-$j", "pseudo $i-$j", bookId = idBook.toInt())
                                    booksViewModel.addComment(comment)
                                    Log.d("BDD", "insert comment $i-$j,  pseudo $i-$j, bookId = ${idBook.toInt()}")
                                }
                            }
                        }
                    }
                    AfficheListeLivres(booksViewModel)
                }
            }
        }
    }
}

@Composable
fun AfficheListeLivres(vm: BooksViewModel) {
    // on écoute le flux issu du viewmodel
    val books by vm.books.collectAsState(initial = emptyList())

    var lastId: Long by rememberSaveable { mutableStateOf(-1) }

    Column {
        val scope = rememberCoroutineScope()
        Button(
                // pour le moment, l'ajout est en dur. A terme, il fera appel à (au choix) une
                // activité, un composable, une boite de dialogue
                onClick = {
                    scope.launch {
                        val idBook: Long = vm.addBook(Book(0, "roro", "titi"))
                        lastId = idBook

                        Log.d("BDD", "ajout du livre n° $idBook")

                        for (j in 1..3) {
                            val comment = Comment(0, "comment $idBook-$j", "pseudo $idBook-$j", bookId = idBook.toInt())
                            vm.addComment(comment)
                            Log.d("BDD", "insert comment $idBook-$j,  pseudo $idBook-$j, bookId = ${idBook.toInt()}")


                        }
                    }
                },
                modifier = Modifier.padding(16.dp)
        ) {
            Text(text = "Cliquez-moi pour ajouter un livre")
        }
        Button(
                enabled = (lastId > 0),
                // pour affcher les comentaires (juste pour test)
                onClick = {
                    scope.launch {
                        // pour traiter retour de Map

                        val bookWithComments = vm.getBookWithComments(lastId)

                        for ((book, comments) in bookWithComments) {
                            // book contient l'objet Book
                            // comments contient une liste d'objets Comment associés au livre

                            // Vous pouvez accéder aux propriétés de Book, par exemple :
                            val bookTitle = book.title
                            val bookAuthor = book.author

                            Log.d("BDD2", "Commentaires pour le livre $lastId = ${book.id}, auteur ${book.author}, titre ${book.title}")

                            // Et parcourir les commentaires si nécessaire :
                            for (comment in comments) {
                                val commentText = comment.comment
                                val commentPseudo = comment.pseudo

                                Log.d("BDD2", "Commentaires : $commentText, pseudo : $commentPseudo ")

                            }

                        }
                    }
                },
                modifier = Modifier.padding(16.dp)
        ) {
            Text(text = "comments ?")
        }
        LazyColumn(
                modifier = Modifier.fillMaxSize()
        ) {
            items(books) { book ->
                Row(modifier = Modifier.fillMaxWidth()) {
                    Column {
                        Text(book.id.toString())
                        Text(book.title)
                        Text(book.author)
                    }
                }
            }
        }
    }
}
